# My Notepad

This is a simple version of the app notepad.  

Implemented functionalities are:
*  cut
*  copy
*  undo
*  redo
*  paste
*  paste with deleting from stack (previous copied text is available for paste)
*  statistics plugin
*  first letter capitalized plugin


Plugins are loaded dynamically so more can be added in directory plugins and will 
be correctly loaded and shown in the program.