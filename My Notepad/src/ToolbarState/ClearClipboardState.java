package ToolbarState;

import javax.swing.JMenuBar;

import EditorPackage.TextEditor;

public class ClearClipboardState implements AbstractState {
	
	String nameState;
	
	public ClearClipboardState() {
		nameState="ClearClipboard";
	}

	@Override
	public void handle(TextEditor editor) {
		JMenuBar menuBar= editor.getMyMenuBar();
	 	menuBar.getMenu(1).getItem(4).setEnabled(true);
		menuBar.getMenu(1).getItem(3).setEnabled(true);
		menuBar.getMenu(1).getItem(8).setEnabled(true);
		menuBar.getMenu(1).getItem(5).setEnabled(false);
		menuBar.getMenu(1).getItem(6).setEnabled(false);
		
	}

	

}
