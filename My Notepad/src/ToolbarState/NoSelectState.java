package ToolbarState;
import javax.swing.JMenuBar;

import EditorPackage.TextEditor;

public class NoSelectState implements AbstractState {

String nameState;
	
	public NoSelectState() {
		nameState="NoSelect";
	}
	@Override
	public void handle(EditorPackage.TextEditor editor) {
		JMenuBar menuBar= editor.getMyMenuBar();
	 	menuBar.getMenu(1).getItem(4).setEnabled(false);
		menuBar.getMenu(1).getItem(3).setEnabled(false);
		menuBar.getMenu(1).getItem(8).setEnabled(false);
		menuBar.getMenu(1).getItem(5).setEnabled(true);
		menuBar.getMenu(1).getItem(6).setEnabled(true);
		
	}

	

}
