package ToolbarState;

import javax.swing.JMenuBar;

import EditorPackage.TextEditor;

public class AllPossibleState implements AbstractState {
String nameState;
	
	public AllPossibleState() {
		nameState="AllPossible";
	}

	@Override
	public void handle(TextEditor editor) {
		JMenuBar menuBar= editor.getMyMenuBar();
	 	menuBar.getMenu(1).getItem(4).setEnabled(true);
		menuBar.getMenu(1).getItem(3).setEnabled(true);
		menuBar.getMenu(1).getItem(8).setEnabled(true);
		menuBar.getMenu(1).getItem(5).setEnabled(true);
		menuBar.getMenu(1).getItem(6).setEnabled(true);

	}

}
