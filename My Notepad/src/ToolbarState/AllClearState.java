package ToolbarState;

import java.awt.MenuBar;

import javax.swing.JMenuBar;

public class AllClearState implements AbstractState {
	String nameState;
	
	public AllClearState() {
		nameState="AllClear";
	}
	@Override
	public void handle(EditorPackage.TextEditor editor) {
		JMenuBar menuBar= editor.getMyMenuBar();
		 	menuBar.getMenu(1).getItem(4).setEnabled(false);
			menuBar.getMenu(1).getItem(3).setEnabled(false);
			menuBar.getMenu(1).getItem(8).setEnabled(false);
			menuBar.getMenu(1).getItem(5).setEnabled(false);
			menuBar.getMenu(1).getItem(6).setEnabled(false);
		
	}
}
