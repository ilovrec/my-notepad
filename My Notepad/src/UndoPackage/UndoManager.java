package UndoPackage;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import EditActionConcrete.EditAction;


public class UndoManager {
	
	// dodat provjere za stackove da nisu prazni 
	
	

	private static UndoManager undoManagerInstc = new UndoManager(); 
	List<UndoRedoObserver> undoObservers;
	Stack<EditAction> undoStack; 
	Stack<EditAction> redoStack; 
	
	public static UndoManager instance() {
		return undoManagerInstc;
	}
	
	private UndoManager() {
		undoStack= new Stack<>();
		redoStack= new Stack<>();
		undoObservers= new ArrayList<>();
	}
	
	public EditAction undo() {
		if(undoStack.isEmpty())
			return null;
		
		EditAction action= undoStack.pop();
		System.err.println("Pozvan undo: "+action.getClass());
		redoStack.push(action);
	
		notifyUndoObserver();
		return action;
	}
	
	public void push(EditAction c) {
		System.err.println("Dodan na undo:" +c.getClass());
		
		redoStack.clear();
		
		undoStack.push(c);
		notifyUndoObserver();
	}
	
	public EditAction redo() {
		if(redoStack.isEmpty())
			return null;
		
		EditAction action= redoStack.pop();
		System.err.println("Pozvan redo: "+action.getClass());
		undoStack.push(action);

		notifyUndoObserver();
		return action;
	}
	
	public void addUndoObserver(UndoRedoObserver obs) {
		this.undoObservers.add(obs);
	}
	
	public void removeUndoObserver(UndoRedoObserver obs) {
		this.undoObservers.remove(obs);
	}
	
	public void notifyUndoObserver() {
		boolean empty_redo= this.redoStack.isEmpty();
		boolean empty_undo= this.undoStack.isEmpty();
		for(UndoRedoObserver obs: this.undoObservers) {
		
			obs.updateUndoStatus(empty_redo, empty_undo);
		}
	}
	
	public Stack<EditAction> getUndoStack() {
		return undoStack;
	}

	public void setUndoStack(Stack<EditAction> undoStack) {
		this.undoStack = undoStack;
	}
}

