package UndoPackage;

public interface UndoRedoObserver {
	public void updateUndoStatus(boolean empty_redo, boolean empty_undo);
}
