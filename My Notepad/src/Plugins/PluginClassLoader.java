package Plugins;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;

public class PluginClassLoader extends ClassLoader {
	String path;
	
	public PluginClassLoader() {
		path="C:\\Users\\Iva i Mia\\Desktop\\iva-faks\\ooup\\lab3\\2. notepad tudi\\src\\Plugins\\conretePlugins";
		
	}
	
	public List<Object>  loadClass() {
		List<Class<?>> all_plugins= PluginClassLoader.getClassesInPackage("\\Plugins\\conretePlugins");
		List<Object> objects = new ArrayList<>();
		for(Class<?> cl: all_plugins) {
			try {
				System.out.println("Plugin je ovdje");
				objects.add(cl.newInstance());
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return objects;
	}
	
	public static final List<Class<?>> getClassesInPackage(String packageName) {
	    //String path = packageName.replaceAll("\\.", File.separator);
		String path= "\\Plugins\\conretePlugins";
	    List<Class<?>> classes = new ArrayList<>();
	    String[] classPathEntries = System.getProperty("java.class.path").split(
	            System.getProperty("path.separator")
	    );
	    System.out.println("Class loader path: "+System.getProperty("java.class.path"));

	    String name;
	    for (String classpathEntry : classPathEntries) {
	    	System.out.println("class path entry:" +classpathEntry);
	        if (classpathEntry.endsWith(".jar")) {
	            File jar = new File(classpathEntry);
	            try {
	                JarInputStream is = new JarInputStream(new FileInputStream(jar));
	                JarEntry entry;
	                while((entry = is.getNextJarEntry()) != null) {
	                    name = entry.getName();
	                    if (name.endsWith(".class")) {
	                        if (name.contains(path) && name.endsWith(".class")) {
	                            String classPath = name.substring(0, entry.getName().length() - 6);
	                            classPath = classPath.replaceAll("[\\|/]", ".");
	                            classes.add(Class.forName(classPath));
	                        }
	                    }
	                }
	            } catch (Exception ex) {
	                // Silence is gold
	            }
	        } else {
	            try {
	                File base = new File(classpathEntry + File.separatorChar + path);
	                System.out.println("Base folder: "+base.getPath());
	                for (File file : base.listFiles()) {
	                	
	                    name = file.getName();
	                    if (name.endsWith(".class")) {
	                    	 System.out.println("Naden class file:"+packageName + "\\" + name);
	                        name = name.substring(0, name.length() - 6);
	                        classes.add(Class.forName("Plugins.conretePlugins." + name));
	                    }
	                }
	            } catch (Exception ex) {
	                // Silence is gold
	            }
	        }
	    }

	    return classes;
	}
	
}
