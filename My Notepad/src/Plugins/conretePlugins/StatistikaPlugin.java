package Plugins.conretePlugins;

import javax.swing.JOptionPane;

import EditorPackage.ClipboardStack;
import EditorPackage.TextEditorModel;
import Plugins.Plugin;
import UndoPackage.UndoManager;

public class StatistikaPlugin implements Plugin {
	String name;
	String description;
	

	public StatistikaPlugin() {
		super();
		this.name = "StatistikaPlugin";
		this.description = "Returns text statistics like number of rows, words and letters";
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public void execute(TextEditorModel model, UndoManager undoManager, ClipboardStack clipboardStack) {
		int rows= model.getLines().size();
		int letters=0, words=0;
		
		for(String line: model.getLines()) {
			String[] split_blank= line.split("\\s+");
			words+=split_blank.length;
			for(int i=0; i<line.length(); i++) {
				if(Character.isLetter(line.charAt(i))) {
					letters++;
				}
			}
		}
		
		String prikaz= new String("Broj redaka: " +rows+"\nBroj rijeci:"+words+"\nBroj slova:"+letters);
		JOptionPane.showMessageDialog(null, prikaz);

	}

}
