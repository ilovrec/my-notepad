package Plugins.conretePlugins;



import java.util.List;

import EditorPackage.ClipboardStack;
import EditorPackage.TextEditorModel;
import Plugins.Plugin;
import UndoPackage.UndoManager;

public class VelikoSlovoPlugin  implements Plugin{
	String name;
	String description;
	public VelikoSlovoPlugin() {
		super();
		this.name = "Uppercase letter";
		this.description = "Every word starts with an uppercase letter";
	}
	@Override
	public String getName() {
		return name;
	}
	@Override
	public String getDescription() {
		return description;
	}
	@Override
	public void execute(TextEditorModel model, UndoManager undoManager, ClipboardStack clipboardStack) {
		List<String> all_lines= model.getLines();
		System.out.println("Sve linije: "+all_lines.toString());
		for(int col=0;col< model.getLines().size(); col++) {
			String line= model.getLines().get(col);
			String[] split_blank= line.split("\\s+");
			String uppercase_line="";
			
			for(String word: split_blank) {
				if(word.length()>0) {
				word =Character.toUpperCase(word.charAt(0)) + word.substring(1);
				uppercase_line+=" "+word;}
			}
			
			all_lines.remove(col);
			System.out.println("Dodaje se linija: "+uppercase_line);
			all_lines.add(col, uppercase_line);
		
		}
		
	}
		
	
}
