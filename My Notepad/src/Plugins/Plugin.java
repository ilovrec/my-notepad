package Plugins;

import EditorPackage.ClipboardStack;
import EditorPackage.TextEditorModel;
import UndoPackage.UndoManager;

public interface Plugin {
	  public String getName(); // ime plugina (za izbornicku stavku)
	  public String getDescription(); // kratki opis
	  public void execute(TextEditorModel model, UndoManager undoManager, ClipboardStack clipboardStack);
	}
