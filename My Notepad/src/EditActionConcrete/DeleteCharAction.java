package EditActionConcrete;

import java.util.ArrayList;
import java.util.List;

import EditorPackage.Location;
import EditorPackage.TextEditorModel;

public class DeleteCharAction implements EditAction {
	public char getC() {
		return c;
	}

	public void setC(char c) {
		this.c = c;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	private char c;
	private Location location;
	private TextEditorModel model;
	private boolean before;  //if its not before then its after
	private  List<String>  lines_before;
	private  List<String>  lines_after;
	
	
	public DeleteCharAction(char c, Location location, TextEditorModel model, List<String> lines_before, boolean before) {
		super();
		System.out.println("Stvoren delCharAct na lokaciji: " +location.getY());
		this.c = c;
		this.location = new Location(location.getX(),location.getY());
		this.model = model;
		this.lines_before= new ArrayList<>();
		this.lines_before.addAll(lines_before);
		this.lines_after= new ArrayList<>();
		this.before=before;
	}
	
	public void addLineAfter( List<String>  line) {
		this.lines_after.addAll(line);
	}

	@Override
	public void execute_do() {
		System.out.println("Postavi nove linije");
		model.setCursorLocation(new Location(location.getX(), location.getY()));
		model.setLines(lines_after);
		
		
	}

	@Override
	public void execute_undo() {
		System.out.println("Postavi stare linije");
		//System.out.println("Zamjeni redak "+location.getY()+" napisano:"+line_after+" s linijom:" +line_before);
		model.setLines(lines_before);
		model.setCursorLocation(new Location(location.getX(), location.getY()));
		


	}

	

}
