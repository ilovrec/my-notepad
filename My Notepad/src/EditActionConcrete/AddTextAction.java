package EditActionConcrete;

import EditorPackage.Location;
import EditorPackage.LocationRange;
import EditorPackage.TextEditorModel;

public class AddTextAction implements EditAction{
	private String text;
	private Location location;
	private TextEditorModel model;
	public AddTextAction(String text, Location location, TextEditorModel model) {
		super();
		this.text = text;
		this.location = new Location(location.getX(), location.getY());
		this.model = model;
	}
	@Override
	public void execute_do() {
		model.setCursorLocation(new Location(location.getX(), location.getY()));
		model.insert(text, true);
		
	}
	
	@Override
	public void execute_undo() {
		String[] split_newline= text.split("\\r?\\n");
		System.out.println("Tekst koji se brise:" +text);
		System.out.println("Prva linija: "+ model.getLines().get(location.getY()));
		int end_y= location.getY()+split_newline.length-1;
		int end_x = split_newline[split_newline.length-1].length();
		if(text.length()==1) {
			location= new Location(location.getX(), location.getY());
		}  
		if(split_newline.length==1)
			end_x+= location.getX();
		System.err.println("Lokacija početka umetanja: "+location.getX()+", "+location.getY());
		System.err.println("Lokacija kraja umetanja: "+end_x +", "+end_y);
	
		Location end= new Location(end_x, end_y);
		LocationRange range= new LocationRange(location,end);
		model.setSelectionRange(range);
		model.DeleteRange(range, true);
		
	}
	
	
	
	
}
