package EditActionConcrete;

import EditorPackage.Location;
import EditorPackage.TextEditorModel;

public class AddNewlineAction implements EditAction {
	private Location loc;
	private TextEditorModel model;
	

	@Override
	public void execute_do() {
		model.setCursorLocation(new Location(loc.getX(), loc.getY()));
		model.insertNewLine(true);

	}

	public AddNewlineAction(Location loc, TextEditorModel model) {
		super();
		this.loc = new Location(loc.getX(),loc.getY());
		this.model = model;
	}

	@Override
	public void execute_undo() {
		model.setCursorLocation(new Location(loc.getX(), loc.getY()));
		model.deleteAfter(true);

	}

}
