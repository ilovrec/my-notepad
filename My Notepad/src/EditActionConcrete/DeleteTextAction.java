package EditActionConcrete;

import EditorPackage.Location;
import EditorPackage.LocationRange;
import EditorPackage.TextEditorModel;

public class DeleteTextAction implements EditAction{
	private String text;
	private LocationRange range;
	private TextEditorModel model;

	public DeleteTextAction(String text, LocationRange range, TextEditorModel model) {
		super();
		this.text = text;
		Location start = new Location(range.getStart().getX(), range.getStart().getY());
		Location end = new Location(range.getEnd().getX(), range.getEnd().getY());
		this.range = new LocationRange(start, end);
		this.model= model;
	}



	@Override
	public void execute_do() {
		//model.setSelectionRange(this.range);
		Location start = new Location(range.getStart().getX(), range.getStart().getY());
		Location end = new Location(range.getEnd().getX(), range.getEnd().getY());
		model.DeleteRange( new LocationRange(start, end), true);
		
	}

	@Override
	public void execute_undo() {
		Location start = new Location(range.getStart().getX(), range.getStart().getY());
		System.err.println("\n\n Undo i dodaje se nazad tekst:" +text);
		model.setCursorLocation(start);
		model.insert(text, true);
		
	}

}
