package EditorPackage;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import EditActionConcrete.AddNewlineAction;
import EditActionConcrete.AddTextAction;
import EditActionConcrete.DeleteCharAction;
import EditActionConcrete.DeleteTextAction;
import EditActionConcrete.EditAction;
import UndoPackage.UndoManager;

public class TextEditorModel {
	

	private List<String> lines;
	private LocationRange selectionRange;
	private Location cursorLocation;
	private List<CursorObserver> cursorObservers;
	private List<TextObserver> textObservers;
	private ClipboardStack clipboard;
	private UndoManager undo_man;
	
	
	public TextEditorModel(String text) {
		this.undo_man= UndoManager.instance();
		clipboard= new ClipboardStack();
		cursorObservers=new LinkedList<>();
		textObservers=new LinkedList<>();
		selectionRange= null;
		cursorLocation= new Location(0,0);
		this.lines= new ArrayList<>();
		String[] split_text= text.split("\\r?\\n");
		for(String line: split_text) {
			lines.add(line);
		}
	}
	
	public UndoManager getUndo_man() {
		return undo_man;
	}

	public void setUndo_man(UndoManager undo_man) {
		this.undo_man = undo_man;
	}
	
	public void resetModel( List<String> text) {
		selectionRange= null;
		cursorLocation= new Location(0,0);
		this.lines= text;
		
	}
	
	 public List<String> getLines() {
		
		return lines;
	}

	public void setLines(List<String> lines) {
		this.lines = lines;
		this.notifyTextObservers();
	}
	
	public void clearLines(boolean undo) {
		  int y= getLines().size()-1;
          int x= getLines().get(y).length();
          Location end_lines= new Location(x,y);
          Location start_lines= new Location(0,0);
          LocationRange range= new LocationRange(start_lines, end_lines);
          this.setSelectionRange(range);
         
          if(!undo) {
          String deleted_text= this.selectionToString();
			EditAction delete= new DeleteTextAction(deleted_text, range,this);
			this.undo_man.push(delete);
			System.err.println("Brise se:"+deleted_text);
          }
          this.setSelectionRange(null);
          this.cursorLocation= new Location(0,0);
          this.lines.clear();
          this.lines.add("");
          
		
	}

	public LocationRange getSelectionRange() {
		return selectionRange;
	}

	public void setSelectionRange(LocationRange selectionRange) {
		this.selectionRange = selectionRange;
		this.notifyTextObservers();
	}

	public Location getCursorLocation() {
		return cursorLocation;
	}

	public void setCursorLocation(Location cursorLocation) {
		this.cursorLocation = cursorLocation;
	}

	public Iterator allLines(){
	     return this.lines.iterator();
	 }
	
	public Iterator linesRange(int index1, int index2) {
	      return this.lines.subList(index1, index2).iterator();
	}
	
	public void addCursorObserver(CursorObserver obs) {
		this.cursorObservers.add(obs);
	}
	
	public void removeCursorObserver(CursorObserver obs) {
		this.cursorObservers.remove(obs);
	}
	
	public void notifyCursorObservers() {
		for(CursorObserver obs: this.cursorObservers) {
			obs.updateCursorLocation(cursorLocation);
		}
	}
	
	public void addTextObserver(TextObserver obs) {
		this.textObservers.add(obs);
	}
	
	public void removeTextObserver(TextObserver obs) {
		this.textObservers.remove(obs);
	}
	
	public void notifyTextObservers() {
		for(TextObserver obs: this.textObservers) {
			obs.updateText();
		}
	}
	
	public void moveCursorUp() {
		int y= this.cursorLocation.getY()-1;
		int x= this.cursorLocation.getX();
		if(y>=0) {
			this.cursorLocation.setY(y);
		if(x>this.lines.get(y).length()-1) {
			x=this.lines.get(y).length()-1;
			cursorLocation.setX(x);
		}
		}
		this.notifyCursorObservers();
	}
	
	public void moveCursorDown() {
		int y= this.cursorLocation.getY()+1;
		int x= this.cursorLocation.getX();
		if(y<= this.lines.size()-1) {
			cursorLocation.setY(y);
		if(x>this.lines.get(y).length()-1) {
			x=this.lines.get(y).length()-1;
			cursorLocation.setX(x);
		} 
		}
		this.notifyCursorObservers();
	}
	
	public void moveCursorLeft() {
		int x= this.cursorLocation.getX()-1;
		if(x<0) {
			int y= this.cursorLocation.getY()-1;
			if(y>=0) {
				x= this.lines.get(y).length()-1;
				this.cursorLocation.setY(y);
			}
			else
				x=0;
		}
		this.cursorLocation.setX(x);
		this.notifyCursorObservers();
	}
	
	public void moveCursorRight() {
		int x= this.cursorLocation.getX()+1;
		int y=cursorLocation.getY();
		if(x >= this.lines.get(y).length()) {
			if(y== lines.size()-1)
				x= this.lines.get(y).length();
			else 
				x=0;
			if(y+1 < this.lines.size())
				cursorLocation.setY(y+1);
		}	
		this.cursorLocation.setX(x);
		this.notifyCursorObservers();
	}
	
	public void replaceLine(Location loc, String line, boolean before) {
	
		int y= loc.getY();
		System.out.println("Zamjenjujem "+lines.get(y)+" s " +line);
		this.lines.remove(y);
		if(line==null) return ;
		this.lines.add(y, line);
		if(before)
			this.moveCursorRight();
	}
	
	public void deleteBefore(boolean undo) {
		int y= cursorLocation.getY();
		int x= cursorLocation.getX();
		char c = 0;
		if(x>0) {
			 c= lines.get(y).charAt(x-1);
			 System.err.println("Brise se znak: "+c);}
		String line= lines.get(y);
		DeleteCharAction delete=null;
		
		if(!undo) {

			delete= new DeleteCharAction(c, cursorLocation, this, lines, true);
			this.undo_man.push(delete);
		}
		
		
		if(x==0) {
			if(y-1>=0) {
				y-=1;
				String previous= lines.get(y);
				line= previous+line;
				lines.remove(y+1);
			}
			else return;
		}
		else {
			line= line.substring(0, x-1) + line.substring(x);
		}
		
		
		lines.remove(y);
		if(line.length()>0)
			lines.add(y, line);
		
		if(delete!=null) {
			if(line.length()<1)
				delete.addLineAfter(null);
			else delete.addLineAfter(lines);
		}
		this.moveCursorLeft();
		this.notifyTextObservers();
		
	}
	
	public void deleteAfter(boolean undo) {
		
		
		int y= cursorLocation.getY();
		int x= cursorLocation.getX();
		char c=0;
		if(x>0) {
			 c= lines.get(y).charAt(x-1);
			 System.err.println("Brise se znak: "+c);}
		String line=lines.get(y);
		DeleteCharAction delete = null;
		if(!undo) {
			 delete= new DeleteCharAction(c, cursorLocation, this, lines, false);
			this.undo_man.push(delete);
		}
	
		
		if(x==line.length()) {
			if(y+1<lines.size()) {
				String next= lines.get(y+1);
				line= line+next;
				lines.remove(y+1);
			}
			else return;
		}else if(x==line.length()-1)
			line= line.substring(0, x);
		else {
			line= line.substring(0, x)+line.substring(x+1);
		}
		lines.remove(y);
		if(line.length()>0)
			lines.add(y, line);
		
		if(delete!=null) {
			
			delete.addLineAfter(lines);
		}
		this.notifyTextObservers();
	}
	
	public void DeleteRange(LocationRange range, boolean undo) {
		int start_x= range.getStart().getX();
		int start_y= range.getStart().getY();
		int end_x= range.getEnd().getX();
		int end_y= range.getEnd().getY();
		List<String> new_lines= new ArrayList<>();
		String new_element="";
		boolean element_added=false;
		
		
		if(!undo) {
			String deleted_text= this.selectionToString();
			EditAction delete= new DeleteTextAction(deleted_text, range,this);
			this.undo_man.push(delete);
			System.err.println("Brise se:"+deleted_text);
		}
		
		
		for(int col=0;col< lines.size(); col++) {
			String line= lines.get(col);
			if(col>= start_y && col<= end_y) {
			if(col == start_y && col== end_y) {
				new_element= line.substring(0, start_x)+line.substring(end_x);
				element_added=true;
				new_lines.add(new_element);
			}
			else if(col== start_y) {
				new_element= line.substring(0,start_x);
			}
			else if(col> start_y && col< end_y)
				line= null;
			else if( col == end_y)
				new_element+= line.substring(end_x);
			
			
			}
			else {
			
			if(col>end_y && !element_added) {
				new_lines.add(new_element);
				element_added=true;
			}
			new_lines.add(line);
			}
			
		}
		if(!element_added) {
			new_lines.add(new_element);
		}
		this.selectionRange=null;
		this.cursorLocation.setX(start_x);
		this.cursorLocation.setY(start_y);
		this.lines=new_lines;
		if(lines.isEmpty())  lines.add("");
		this.notifyTextObservers();

	}
	
	public void insert(char c, boolean undo) {
		String line= lines.get(this.cursorLocation.getY());
		
		if(!undo) {
			EditAction insert= new AddTextAction(Character.toString(c), cursorLocation,this);
			this.undo_man.push(insert); 
		}
		
		/*if(this.getSelectionRange()!=null)
			this.DeleteRange(this.getSelectionRange()); */
		line= line.substring(0, cursorLocation.getX())+ c + line.substring(cursorLocation.getX());
		lines.remove(cursorLocation.getY());
		lines.add(cursorLocation.getY(), line);
		this.moveCursorRight();
		this.notifyTextObservers();
	}
	
	public void insert(String text, boolean undo) {
		
		// boolean undo - akcija se provodi kao dio undo/redo,
		//   			  vec obavljeno potrebno premjestanje s ili na stog
		
		if(!undo) {
			EditAction insert= new AddTextAction(text, cursorLocation,this);
			this.undo_man.push(insert); 
		}
		
		if(lines.isEmpty())
			lines.add("");
		if(this.cursorLocation.getY()>lines.size())
			this.cursorLocation.setY(lines.size()-1);
		if(this.cursorLocation.getX()<0)
			this.cursorLocation.setX(0);
		String line= lines.get(this.cursorLocation.getY());
		String line_before= line.substring(0, cursorLocation.getX());
		String line_after = line.substring(cursorLocation.getX());
		String[] split_newline= text.split("\\r?\\n");
		List<String> new_lines= new ArrayList<>();
		
		for(int col=0;col< lines.size(); col++) {
			String old_line= lines.get(col);
			if(col == cursorLocation.getY()) {
				String whole_line= line_before+split_newline[0];
		
				if(split_newline.length==1) {
					whole_line+= line_after;
					new_lines.add(whole_line);
				}
				else {
				int i=1;
				new_lines.add(whole_line);
				for(i=1; i<split_newline.length-1 ; ++i){
					new_lines.add(split_newline[i]);
				}
				whole_line= split_newline[i]+line_after;
				if(lines.size()>col+1) {
				if(!text.endsWith("\n")) {
					whole_line+= lines.get(++col);
				}
				}
				new_lines.add(whole_line);
				}
				
				
				
				
			}else
			new_lines.add(old_line);
		}
		
		this.lines= new_lines;
		this.notifyTextObservers();
		
	}
	
	public void insertNewLine(boolean undo) {
		List<String> new_lines= new ArrayList<>();
		
		if(!undo) {
			AddNewlineAction add_line= new AddNewlineAction(cursorLocation, this);
			this.undo_man.push(add_line);
		}
		for(int col=0;col< lines.size(); col++) {
			String line= lines.get(col);
			if(col == cursorLocation.getY()) {
				String first_line= line.substring(0, cursorLocation.getX());
				String second_line= line.substring(cursorLocation.getX());
				new_lines.add(first_line);
				if (second_line.isEmpty())
					second_line="";
				new_lines.add(second_line);
			}else {
				new_lines.add(line);
			}
		}
		
		lines= new_lines;
		this.moveCursorRight();
		this.notifyTextObservers();
		
	}
	
	public String selectionToString() {
		LocationRange range= this.getSelectionRange();
		if(range==null) {
			System.err.println("Ni�ta nije selektirano");
			return null;
		}
		int start_x= range.getStart().getX();
		int start_y= range.getStart().getY();
		int end_x= range.getEnd().getX();
		int end_y= range.getEnd().getY();
		String select="", new_element="";
		
		for(int col=0;col< lines.size(); col++) {
			String line= lines.get(col);
			if(col>= start_y && col<= end_y) {
				if(col == start_y && col== end_y) {
					return line.substring(start_x, end_x);
				}
				else if(col== start_y) {
					select= line.substring(start_x);
				}
				else if(col> start_y && col< end_y)
					new_element= line;
				else if( col == end_y)
					new_element= line.substring(0,end_x);
				
				if(!new_element.isEmpty())
					select= select +"\n" +new_element;
				}
			
			if(col> end_y)
				return select;
	
		}
		
		return select;
		
	}
	
	public void copy() {
		String selected= this.selectionToString();
		if(selected==null) {
			System.out.println("Nista nije odabrano za kopiranje");
			return;
		}
		System.err.println("Kopirano:" + selected);	
		clipboard.push(selected);
	}
	
	public void cut() {
		copy();
		this.DeleteRange(selectionRange, false);
	}
	
	public void paste() {
		if(clipboard.isEmpty()) {
			System.out.println("Nema nikakvog kopiranog teksta");
			return;
		}
			
		String to_paste= clipboard.peek();
		this.insert(to_paste, false);
	}
	
	public void pasteAndRemove() {
		if(clipboard.isEmpty()) {
			System.out.println("Nema nikakvog kopiranog teksta");
			return;
		}
		String to_paste= clipboard.pop();
		this.insert(to_paste, false);
	}

	public ClipboardStack getClipboard() {
		return clipboard;
	}

	public void setClipboard(ClipboardStack clipboard) {
		this.clipboard = clipboard;
	}
}
