package EditorPackage;

public class LocationRange {
	private Location start;
	private Location end;
	
	public LocationRange(Location start, Location end) {
		super();
		if(start.isAfter(end)) {
			Location temp= start;
			start= end;
			end=temp;
		}
		this.start = start;
		this.end = end;
	}
	public Location getStart() {
		return start;
	}
	public void setStart(Location start) {
		this.start = start;
	}
	public Location getEnd() {
		return end;
	}
	public void setEnd(Location end) {
		this.end = end;
	}
}
