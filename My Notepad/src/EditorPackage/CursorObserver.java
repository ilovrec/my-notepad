package EditorPackage;

public interface CursorObserver {
	public void updateCursorLocation(Location loc);
}
