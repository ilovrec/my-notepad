package EditorPackage;

public interface TextObserver {
	public void updateText();
}
