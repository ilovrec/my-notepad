package EditorPackage;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class ClipboardStack {
	Stack<String> texts;
	List<ClipboardObserver> clipboardObservers; 
	
	public ClipboardStack() {
		texts= new Stack<>();
		this.clipboardObservers= new ArrayList<>();
	}
	
	public void notifyClipboarObservers() {
		for(ClipboardObserver obs: clipboardObservers) {
			obs.updateClipboard();
		}
	}
	
	public void addClipboarObserver(ClipboardObserver obs) {
		System.err.println("Dodan observer");
		this.clipboardObservers.add(obs);
	}
	
	public void removeClipboarObserver(ClipboardObserver obs) {
		this.clipboardObservers.remove(obs);
	}
	
	public void push(String text) {
		System.err.println("Stavljeno nesto na stog");
		texts.push(text);
		this.notifyClipboarObservers();
	}
	
	public String pop() {
		System.err.println("Micem s stoga");
		String pop= texts.pop();
		if(this.isEmpty())
			System.err.println("Stog je prazan javlja stack");
		this.notifyClipboarObservers();
		return pop;
	}
	
	public boolean isEmpty() {
		return texts.isEmpty();
	}
	
	public String peek() {
		return texts.peek();
	}
	
	public void delete() {
		texts.clear();
		this.notifyClipboarObservers();
	}
}
