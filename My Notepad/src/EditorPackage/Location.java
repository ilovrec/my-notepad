package EditorPackage;

public class Location {
	private int x;
	private int y;
	public int getX() {
		return x;
	}
	public void setX(int x) {
		if(x<0)
			this.x=0;
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		if(y<0)
			this.y=0;
		this.y = y;
	}
	public Location(int x, int y) {
		super();
		if(x<0) x=0;
		if(y<0) y=0;
		this.x = x;
		this.y = y;
	}
	
	public boolean isAfter(Location loc){
        if (this.getY() > loc.getY()){
            return true;
        }else if(this.getY()==  loc.getY()) {
        	if(this.getX()> loc.getX())
        		return true;
        }
        return false;
    }
	
}
