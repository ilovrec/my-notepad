package EditorPackage;


import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class MainBasicGUI extends JFrame{

    public MainBasicGUI() {
        setTitle("Zadatak 2.1");
        setSize(960, 960);
        setVisible(true);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

    }

    @Override
    public void paint(Graphics g){
        super.paint(g);
        int width = getWidth();
        int height = getHeight();
        g.setColor(Color.RED);
        g.drawLine(0, 200, width, 200);
        g.setColor(Color.GREEN);
        g.drawLine(200, 0, 200, height);
      

        g.setColor(Color.BLACK);
        g.setFont(new Font(null, Font.PLAIN, 30));
        g.drawString("Tekst 1", 100, 100);
        g.setFont(new Font(null, Font.ITALIC, 30));
        g.drawString("Ovo je drugi tekst", 200, 200);
    }

    public static void main(String[] args){
        MainBasicGUI window = new MainBasicGUI();
        window.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER ){
                    System.out.println("Enter typed!");
                }
                
            }

            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER ){
                    System.out.println("Enter pressed!");
                    window.setVisible(false);
                    window.dispose();
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER ){
                    System.out.println("Enter released!");
                }

            }
        });

    }
}
