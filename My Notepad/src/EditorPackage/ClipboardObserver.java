package EditorPackage;

public interface ClipboardObserver {
	public void updateClipboard();
}
