package EditorPackage;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.Rectangle2D;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTextArea;
import javax.swing.WindowConstants;

import EditActionConcrete.EditAction;
import Plugins.Plugin;
import Plugins.PluginClassLoader;
import Plugins.conretePlugins.StatistikaPlugin;
import Plugins.conretePlugins.VelikoSlovoPlugin;
import ToolbarState.AbstractState;
import ToolbarState.AllClearState;
import ToolbarState.AllPossibleState;
import ToolbarState.ClearClipboardState;
import ToolbarState.NoSelectState;
import UndoPackage.UndoManager;
import UndoPackage.UndoRedoObserver;

public class TextEditor extends JFrame implements CursorObserver, TextObserver, ClipboardObserver,
				UndoRedoObserver{
	
	

	private TextEditorModel model;
	private Location cursor;
	private JFileChooser file_choose = new JFileChooser();
	private JTextArea textArea;
	private  JMenuBar menuBar ;
	private AbstractState state;
	private JLabel bottom_label;
	
	public TextEditor(TextEditorModel model) {
		this(model.getLines().size());
		System.err.println("Broj linija:" +model.getLines().size());
		this.model=model;
		this.model.getUndo_man().addUndoObserver(this);
		
		model.getClipboard().addClipboarObserver(this);
		this.cursor= new Location(0,0);
		model.addCursorObserver(this);
		model.addTextObserver(this);
		this.file_choose = new JFileChooser();
		this.textArea = new JTextArea(20, 60);
		this.state= new AllClearState();
		model.setSelectionRange(new LocationRange(new Location(0,0), new Location(0,1)));
		
        
		
	}
	
	public JMenuBar getMyMenuBar() {
		return menuBar;
	}


	public void setMenuBar(JMenuBar menuBar) {
		this.menuBar = menuBar;
	}

	
	
	public TextEditor(int num_lines) {
		 setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
	       System.err.println("Broj linija:"+num_lines);
		 this.bottom_label= new JLabel("");
		 this.bottom_label.setText("Cursor: row - 0, column - 0        Number of rows:"+ num_lines);
	        add(bottom_label);
	        bottom_label.setVerticalAlignment(JLabel.BOTTOM);
	        
	        
	       
	        
		  menuBar = new JMenuBar();
	        setJMenuBar(menuBar);
	        JMenu file = new JMenu("File");
	        JMenu edit = new JMenu("Edit");
	        JMenu move = new JMenu("Move");
	        JMenu plugins = new JMenu("Plugins");
	        menuBar.add(file);
	        file.add(Open);
	        file.add(Save);
	        file.addSeparator();
	        file.add(Exit); 
	        
	        menuBar.add(edit);
	        edit.add(Undo);
	        edit.add(Redo);
	        edit.addSeparator();
	        
	        edit.add(Cut);
	        edit.add(Copy);
	        edit.add(Paste);
	        edit.add(PasteAndRemove);
	        edit.addSeparator();
	        edit.add(DeleteSelection);
	        edit.add(ClearDocument);
	         
	        menuBar.add(move);
	        move.add(CursorStart);
	        move.add(CursorEnd); 
	        
	        
	        menuBar.getMenu(1).getItem(0).setEnabled(false);
	        menuBar.getMenu(1).getItem(1).setEnabled(false);
	        
	        PluginClassLoader plugin_loader= new PluginClassLoader();
	        List<Object> loaded_plugins= plugin_loader.loadClass();
	        System.out.println("Pluginovi:" +loaded_plugins.toString());
	        menuBar.add(plugins);
	        
	        for(Object obj_plugin: loaded_plugins) {
	        	
	        	Plugin plug= (Plugin) obj_plugin;
	        	JMenu plugin_menu= new JMenu();
	        	plugin_menu.setText(plug.getName());
	        	Plugin_Action= new AbstractAction(plug.getName()) {
	                @Override
	                public void actionPerformed(ActionEvent e) {
	                	System.out.println("Izvodenje akcije plugina:" +plug.getName());
	                  plug.execute(model, model.getUndo_man(), model.getClipboard());
	                  repaint();
	                }
	            };
	        	plugins.add(Plugin_Action);
	        	
	        	
	        	
	        	//plugins.add(plugin_menu);
	        }
	        
	        
	        
	        pack();
	        setLocation(100, 100);
	        setSize(400, 900);
	        setVisible(true);
	        
	        this.addKeyListener(new KeyListener() {
	            Location start_select;
	            
	            @Override
	            public void keyTyped(KeyEvent e) {
	                return;
	            }

	            @Override
	            public void keyPressed(KeyEvent e) {
	            	
	            	if (e.isControlDown() && e.getKeyCode() == KeyEvent.VK_C) {
	            		  model.copy();
	            		  model.setSelectionRange(null);
	            		  repaint();
	            	  }
	            	  else if (e.isControlDown() && e.getKeyCode() == KeyEvent.VK_X) {
	            		 System.err.println("Pozvan cut");
	            		  model.cut();
	            		  model.setSelectionRange(null);
	            		  repaint();
	            	  }
	            	  else if (e.isControlDown() && e.isShiftDown() &&
	                        e.getKeyCode() == KeyEvent.VK_V) {
	            		  
	            		  model.pasteAndRemove();
	            		  model.setSelectionRange(null);
	            		  repaint();
	            	  }
	            	
	            	  else if (e.isControlDown() && e.getKeyCode() == KeyEvent.VK_V) {
	            		  model.paste();
	            		  model.setSelectionRange(null);
	            		  repaint();
	            	  }
	            	
	            	  else if (e.isControlDown() && e.getKeyCode() == KeyEvent.VK_Z) {
	            		  EditAction action= model.getUndo_man().undo();
	            		  System.out.println("\n\n Nova undo naredba");
	            		  if (action== null)
	                		  return;
	            		  action.execute_undo();
	            	  }
	            	
	            	  else if (e.isControlDown() && e.getKeyCode() == KeyEvent.VK_Y) {
	            		  EditAction action= model.getUndo_man().redo();
	            		  if (action== null)
	                		  return;
	            		  action.execute_do();
	            	  }
	            	
	            	  else if ((e.getKeyCode() >= 65 && e.getKeyCode()<= 90)
	                          || (e.getKeyCode() >= 96 && e.getKeyCode() <= 105)
	                          || e.getKeyCode() == KeyEvent.VK_SPACE 
	                          || (e.getKeyCode() >= 48 && e.getKeyCode() <= 57)) {
	            		  
	            		  if(model.getSelectionRange()!=null){
		            			model.DeleteRange(model.getSelectionRange(),false);
		            		}
	                      model.insert(e.getKeyChar(),false);
	                  }
	            	
	            	
	            	if (e.getKeyCode() == KeyEvent.VK_KP_UP ||
	                        e.getKeyCode() == KeyEvent.VK_UP){
	                    	model.moveCursorUp();
	                        repaint();
	            	}
	            	else if (e.getKeyCode() == KeyEvent.VK_KP_DOWN ||
	                        e.getKeyCode() == KeyEvent.VK_DOWN){
	                    	model.moveCursorDown();
	                        repaint();
	            	}
	            	
	            	else if (e.getKeyCode() == KeyEvent.VK_KP_RIGHT ||
	                        e.getKeyCode() == KeyEvent.VK_RIGHT){
	                    	model.moveCursorRight();
	                        repaint();
	            	}
	            	
	            	else if (e.getKeyCode() == KeyEvent.VK_KP_LEFT ||
	                        e.getKeyCode() == KeyEvent.VK_LEFT){
	                    	model.moveCursorLeft();
	                        repaint();
	            	}
	            	
	            	else if(e.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
	            		if(model.getSelectionRange()!=null){
	            			model.DeleteRange(model.getSelectionRange(),false);
	            		}else
	            			model.deleteBefore(false);
	            	}
	            	
	            	else if(e.getKeyCode() == KeyEvent.VK_DELETE) {
	            		if(model.getSelectionRange()!=null){
	            			model.DeleteRange(model.getSelectionRange(),false);
	            		}else
	            			model.deleteAfter(false);
	            	}
	            	
	            	 else if (e.getKeyCode() == KeyEvent.VK_SHIFT){
	            		  int x= cursor.getX();
	            		  int y= cursor.getY();
	            		  start_select= new Location(x,y);
	            	  }
	            	
	            	 
	            	
	            	  else if (e.getKeyCode() == KeyEvent.VK_ENTER){
	                      model.insertNewLine(false);
	                  }
	            	
	            	  
	            }

				@Override
				public void keyReleased(KeyEvent e) {
					 if (e.getKeyCode() == KeyEvent.VK_SHIFT){
						  int x= cursor.getX();
	            		  int y= cursor.getY();
						 Location end_select= new Location(x,y);
						 LocationRange whole_select= new LocationRange(start_select, end_select);
						 model.setSelectionRange(whole_select);
					
					 }
					
				}
	        });
	}
	
	private void markSelected(String line, LocationRange range, Graphics g, int height, int num_row) {
		int start_x= range.getStart().getX();
		int start_y= range.getStart().getY();
		int end_x= range.getEnd().getX();
		int end_y= range.getEnd().getY();
		FontMetrics metrics = g.getFontMetrics();
		String selected_line, nonselected_before="", nonselected_after="";
		if(num_row == start_y && num_row == end_y) {
			nonselected_before= line.substring(0, start_x);
			selected_line= line.substring(start_x,end_x);
		}
		else if(num_row== start_y) {
			selected_line= line.substring(start_x, line.length());
			nonselected_before= line.substring(0, start_x);
		}
		else if( num_row == end_y) {
			nonselected_after= line.substring(end_x+1, line.length());
			selected_line= line.substring(0, end_x+1);
		}
		else {
			selected_line=line;
		}
		//System.out.println("Selektirani tekst: "+selected_line); 
		Rectangle2D selected= metrics.getStringBounds(selected_line, g);
		Rectangle2D before_select= metrics.getStringBounds(nonselected_before, g);
		Rectangle2D after_select= metrics.getStringBounds(nonselected_after, g);
		g.setColor(Color.lightGray);
		g.fillRect(20+ (int) before_select.getWidth(), (int) height- metrics.getAscent(), (int) selected.getWidth(), (int) selected.getHeight());
		g.setColor(Color.BLACK);
		
		
		
	}
	
	 @Override
	    public void paint(Graphics g){
	        super.paint(g);
	        this.updateState();
	        System.out.println("\n\n");
	       
	        Font myFont = new Font(null, Font.PLAIN, 25);
	        g.setFont(myFont);
	        int num_row = 0;
	        int left_margin = 20;
	        int up_margin =90;
	        boolean selection_exists = false;
	   
	        int start_row=0, end_row=0;
	        

	        
	       if (this.model.getSelectionRange() != null){
	            selection_exists = true;
	            start_row= model.getSelectionRange().getStart().getY();
	            end_row= model.getSelectionRange().getEnd().getY();
	            System.out.println("Selekcija je: " +model.selectionToString());
	            
	        }
	       this.state.handle(this);
	       
	      /* if(selection_exists) {
	    	   menuBar.getMenu(1).getItem(4).setEnabled(true);
				menuBar.getMenu(1).getItem(3).setEnabled(true);
				menuBar.getMenu(1).getItem(8).setEnabled(true);
	       }
	       else{
	    	   menuBar.getMenu(1).getItem(4).setEnabled(false);
				menuBar.getMenu(1).getItem(3).setEnabled(false);
				menuBar.getMenu(1).getItem(8).setEnabled(false);
	       }
	       
	       if(model.getClipboard().isEmpty()) {
				menuBar.getMenu(1).getItem(5).setEnabled(false);
				menuBar.getMenu(1).getItem(6).setEnabled(false);
			}
			else {
				menuBar.getMenu(1).getItem(5).setEnabled(true);
				menuBar.getMenu(1).getItem(6).setEnabled(true);
			}  */
	       
	       
	        Iterator<String> iterator_lines = this.model.allLines();
	        if(!iterator_lines.hasNext())  return;
	        String line=iterator_lines.next();
	        while(true){
	        	if(cursor.getY() == num_row) {
	        		//System.out.println("Lokacij kursora: "+cursor.getX()+","+cursor.getY());
	        		int x= cursor.getX();
	        		//System.out.println("Duljina linije: "+ line.length()+", a x cursora:"+ x);
	        		if(line==null)
	        			line="|";
	        		else if(x<=0) line= "|" + line;
	        		else if(x>= line.length())
	        			line= line+ "|";
	        		else
	        		line = line.substring(0, x)+ "|" + line.substring(x, line.length());
	        	}
	        	
	        	if(selection_exists) {
	        		if(num_row >= start_row && num_row <= end_row) {
	        			this.markSelected(line, model.getSelectionRange(),g, up_margin, num_row);
	        		}
	        	}
	            g.drawString(line, left_margin, up_margin);
	            if(!iterator_lines.hasNext()) break;
	            line=iterator_lines.next();
	            up_margin += 30;
	            num_row += 1;
	        }
	        
	        
	 }


	public static void main(String[] args){
		
		TextEditorModel mod= new TextEditorModel("Ovo ce biti dugacak tekst \n s puno \n redaka\n i slova. \n "
				+ "Zapravo nije tako \n dugacko. \n Al treba za ispobati \n pa moram pisat.");
		TextEditor editor= new TextEditor(mod);
	}


	@Override
	public void updateCursorLocation(Location loc) {
		cursor= loc;
		//System.out.println("Namjestam novu lokaciju kursora: "+cursor.getX()+","+cursor.getY());
		this.bottom_label.setText("Cursor: row - "+cursor.getY()+", column - "+cursor.getX()+"        Number of rows:"+ model.getLines().size());
		repaint();
		
	}
	
	@Override
    public void updateText() {
		this.bottom_label.setText("Cursor: row - "+cursor.getY()+", column - "+cursor.getX()+"        Number of rows:"+ model.getLines().size());
        repaint();
    }
	
	
	Action Open = new AbstractAction("Open File") {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (file_choose.showOpenDialog(null) == JFileChooser.APPROVE_OPTION){
                openFile(file_choose.getSelectedFile().getAbsolutePath());
            }
        }
    };
    
    public void openFile(String fileName){
        FileReader reader = null;
        try {
            reader = new FileReader(fileName);
            //textArea.read(reader, null);
           model.resetModel(Files.readAllLines(Paths.get(fileName)));
            reader.close();
            setTitle(fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        repaint();
    }
    
    Action Save = new AbstractAction("Save File") {
        @Override
        public void actionPerformed(ActionEvent e) {
            saveFile();
        }
    };
    
    public void saveFile(){
        if (file_choose.showSaveDialog(null) == JFileChooser.APPROVE_OPTION){
        	PrintWriter writer = null;
            try {
                writer = new PrintWriter(file_choose.getSelectedFile().getAbsoluteFile() + ".txt","UTF-8");
                List<String> all_lines= model.getLines();
                for(String line: all_lines)
                	writer.println(line);
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    
    Action Plugin_Action;

    Action Exit = new AbstractAction("Exit") {
        @Override
        public void actionPerformed(ActionEvent e) {
            System.exit(0);
        }
    };
    
    Action Undo = new AbstractAction("Undo") {
        @Override
        public void actionPerformed(ActionEvent e) {
        	  EditAction action= model.getUndo_man().undo();
        	  if (action== null)
        		  return;
    		  action.execute_undo();
        }
    };

    Action Redo = new AbstractAction("Redo") {
        @Override
        public void actionPerformed(ActionEvent e) {
        	  EditAction action= model.getUndo_man().redo();
        	  if (action== null)
        		  return;
    		  action.execute_do();
        }
    };

    Action Cut = new AbstractAction("Cut") {
        @Override
        public void actionPerformed(ActionEvent e) {
        	 if(model.getSelectionRange()==null) {
             	return;
             }
             model.cut();
             model.setSelectionRange(null);
             repaint();
        }
    };

    Action Copy = new AbstractAction("Copy") {
        @Override
        public void actionPerformed(ActionEvent e) {
            if(model.getSelectionRange()==null) {
            	return;
            }
            model.copy();
            model.setSelectionRange(null);
            repaint();
        }
    };

    Action Paste = new AbstractAction("Paste") {
        @Override
        public void actionPerformed(ActionEvent e) {
        	if(model.getClipboard().isEmpty()) {
        		
        		return;
        	}
        	model.paste();
        	model.setSelectionRange(null);
        	repaint();
        }
    };

    Action PasteAndRemove = new AbstractAction("Paste and Remove") {
        @Override
        public void actionPerformed(ActionEvent e) {
        		if(model.getClipboard().isEmpty()) {
        		
        		return;
        	}
        	model.pasteAndRemove();
        	 model.setSelectionRange(null);
   		  	repaint();
        }
    };

    Action DeleteSelection = new AbstractAction("Delete Selection") {
        @Override
        public void actionPerformed(ActionEvent e) {
        	if(model.getSelectionRange()==null) {
        		return;
        	}
        	model.DeleteRange(model.getSelectionRange(),false);
        	
        }
    };

    Action ClearDocument = new AbstractAction("Clear Document") {
        @Override
        public void actionPerformed(ActionEvent e) {
        	model.clearLines(false);
           model.setCursorLocation(new Location(0,0));
           model.notifyTextObservers();
        }
    };

    Action CursorStart = new AbstractAction("Cursor to Document Start") {
        @Override
        public void actionPerformed(ActionEvent e) {
           model.setCursorLocation(new Location(0,0));
           model.notifyCursorObservers();
        }
    };

    Action CursorEnd = new AbstractAction("Cursor to Document End") {
        @Override
        public void actionPerformed(ActionEvent e) {
            int y= model.getLines().size()-1;
            int x= model.getLines().get(y).length();
            model.setCursorLocation(new Location(x,y));
            model.notifyCursorObservers();
        }
    };

	@Override
	public void updateClipboard() {
		System.err.println("Updata se da je dodano na stog");
		
		
		
	}
	
	public void updateState() {
		if(model.getSelectionRange()==null) {
			if(model.getClipboard().isEmpty())
				this.state= new AllClearState();
			else
				this.state= new NoSelectState();
		}
		else {
			if(model.getClipboard().isEmpty())
				this.state= new ClearClipboardState();
			else 
				this.state= new AllPossibleState();
		}
	}

	@Override
	public void updateUndoStatus(boolean empty_redo, boolean empty_undo) {
		//System.out.println("Updatanje tipki undo:" +empty_undo +" i redo:"+empty_redo);
		/*StatistikaPlugin plug= new StatistikaPlugin();
		plug.execute(model, model.getUndo_man(), model.getClipboard()); */
		
	
		
		if(empty_undo)
			menuBar.getMenu(1).getItem(0).setEnabled(false);
		else 
			menuBar.getMenu(1).getItem(0).setEnabled(true);
		
		if(empty_redo)
			menuBar.getMenu(1).getItem(1).setEnabled(false);
		else 
			menuBar.getMenu(1).getItem(1).setEnabled(true);
		repaint();
	}

	
}
